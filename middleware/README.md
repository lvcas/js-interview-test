# MIDDLEWARE EXERCISE

This excercise has been completed using a simple Middleware class available in `middleware/answer.js`

A vanilla example of this class in action can be seen in `middleware/index.js` which sets an identity, then determines greeting behaviour, and finally greets.