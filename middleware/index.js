const Middleware = require('./answer');


const middleware = new Middleware();

middleware.use(function (next) { // sets identity
    this.identity = {
        name: 'Lucas',
        lastNames: 'Gil Melby'
    };
    next();
});

middleware.use(function (next) { // sets greeting behaviour
    this.greet = () => {
        console.log(`Hi! My name is ${this.identity.name} ${this.identity.lastNames} :)`);
    };
    next();
});

middleware.go(function () { // executes greeting
    this.greet();
});




