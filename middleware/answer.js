class Middleware {
    go(nextStep) { // executes all functions in "queue" and executes 'nextStep' at the end
        nextStep();
    };

    use(func) { // adds function to queue
        this.go = (queue => { // modifies the 'go()' function to "queue" function 'func' (passed as param) so that each functino passed to 'use()' is executed in order and when 'go()' in invoked. Finally the function passed to 'go()' is executed.
            return (following) => {
                queue(func.bind(this, following.bind(this)));
            }
        })(this.go);
    };
}


module.exports = Middleware;
