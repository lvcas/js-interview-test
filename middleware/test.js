var assert = require('assert');
var Middleware = require('./answer');

describe('middleware', function() {
    it('works with a single instance', function(done) {

        var middleware = new Middleware();
        let i = 1;

        middleware.use(function(next) {
            var ctx = this;
            setTimeout(function() {
                ctx.first = i;
                i++;
                next();
            }, 10);
        });

        middleware.use(function(next) {
            var ctx = this;
            setTimeout(function() {
                ctx.second = i;
                i++;
                next();
            }, 10);
        });

        middleware.go(function() {
            assert.equal(this.first, 1);
            assert.equal(this.second, 2);
            done();
        });
    });





    it('works with multiple instances', function(done) {

        var middleware1 = new Middleware();
        var middleware2 = new Middleware();

        middleware1.use(function(next) {
            var ctx = this;
            setTimeout(function() {
                ctx.first = true;
                next();
            }, 10);
        });

        middleware2.use(function(next) {
            var ctx = this;
            setTimeout(function() {
                ctx.second = true;
                next();
            }, 10);
        });

        middleware1.go(function() {
            assert.equal(this.first, true);
            middleware2.go(function() {
                assert.equal(this.second, true);
                done();
            })
        });

    });


});