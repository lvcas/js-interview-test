const memoize = require('./answer');


let calledNormal = 0;
const fact = (n) => {
    calledNormal++;
    if (n <= 1) {
        return 1;
    } else {
        return n * fact(n - 1);
    }
}

let calledMemoize = 0;
const factM = memoize((n) => {
    calledMemoize++;
    if (n <= 1) {
        return 1;
    } else {
        return n * factM(n - 1);
    }
});

fact(10);
fact(15);
fact(12);

factM(10);
factM(15);
factM(12);

console.log(`'fact() function was only called ${calledMemoize} times instead of ${calledNormal} times.`);