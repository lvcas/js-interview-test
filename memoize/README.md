# MEMOIZE EXERCISE

This excercise has been completed using two different approaches which can be found in `middleware/answer.js`

### Complete solution

This option (default option) uses an n-dimentional cache (actually a tree) to store the values computed in each execution. This function is more robust but _can_ also be more costly if the tree gets quite deep.

### Simple solution

A simpler solution has also been completed which yields the same final result for the test cases but _could_ have some limitations depending on the values inputed as params.
> To test this solution comment out lines 32-48 of `memoize/answer.js` and uncomment lines 5-29 from the same file.




A vanilla example of this solution can be seen in `memoize/index.js` which uses previous executions to ease factorial computations.