

const cache = {};

// const memoize = (func) => {
//     return (...args) => {
//         let auxCache = cache;
//         let counter = 0;

//         args.some((arg) => { // Tries to find a cached value in the n-dimentional array (actually tree) using the arguments passed as keys
//             if (!['number', 'string'].includes(typeof arg)) throw 'Only numbers and strings are accepted as parameter.';
//             auxCache = auxCache[arg.toString()];
//             counter++;
//             return auxCache == null;
//         })

//         if (auxCache && args.length === counter) { // value found in cache
//             return auxCache.value;
//         } else { // value not found in cache
//             auxCache = cache;
//             args.forEach((arg) => { // determines where new computed value will have to be saved (finds node of tree)
//                 auxCache[arg.toString()] = {};
//                 auxCache = auxCache[arg.toString()];
//             })
//             auxCache.value = func.apply(null, args); // computes value and stores it in the cache
//             return auxCache.value;
//         }
//     }
// };


const memoize = (func) => {
    const separator = '##' // TODO: Change value to a much "stranger" value to avoid user inputing the same
    return (...args) => {
        let key = '';
        args.forEach((arg) => { // determines key to look for in cache (concatenation of arguments and '##')
            if (!['number', 'string'].includes(typeof arg)) throw 'Only numbers and strings are accepted as parameter.';
            key = `${key}${separator}${arg.toString()}`;
        })

        if (Object.keys(cache).includes(key)) { // value is found in cache
            return cache[key];
        } else { // value is not found in cache
            cache[key] = func.apply(null, args); // computes value and stores it in the cache
            return cache[key];
        }
    }
};

module.exports = memoize;
