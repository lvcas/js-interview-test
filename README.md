# JS Interview Test

Hi,

Following is a set of exercises that we would like you to complete and provide code examples).
As there are plenty of different ways to approach each exercise it is important to understand that we are not looking 
for a specific answer. Each exercise will merely serve as the foundation for a discussion where we can learn about your 
thought process and pick your brain regarding specific decisions that you made implementation-wise

Please review each exercise and get in touch with us if you have any questions or concerns, preferably prior to 
submitting your solutions.

Each exercise has three files:
* index.js: write an example of code that uses your answer.
* test.js: short test suit for the exercise. You can add more tests if needed but the existing ones must pass.
* answer.js: file where you have to write your solution, you can create extra files if needed.

This uses a basic TDD approach so take a look at the test.js file in each directory to see what needs to be implemented,
write an index.js as the solution file.

## Exercises

* Download the code and send us in a zip file or fork the main repository and tell us where we can check the answer.
* Implement all exercises in vanilla javascript without the help of any third party libraries.
* A `README.md` file can be added for each exercise leave some notes about what you did or any comment you want.

### Memoize
Memoization is the programmatic practice of making long recursive/iterative functions run much faster by caching the 
values that the function returns after its initial execution.

Implement a method that calls a function or returns the result if was memoized following the `index.js` and `test.js` requirements.

```
var memoize = require('./answer'); // <- this is the file you make;
   
   function expensiveOperation() {
       console.log('this should be shown once');
       return 22;
   }
   
   let memoized = memoize(expensiveOperation);
   console.log(memoized());
   console.log(memoized());
   
   // the console should show:
   // this should be shown once
   // 22
   // 22
```

More info: https://en.wikipedia.org/wiki/Memoization

### Middleware

Middleware is the programming pattern of providing hooks with a resume callback.

Implement your own pattern following the `index.js` and `test.js` requirements.

```
var Middleware = require('./answer');  // <- this is the file you make;

var middleware = new Middleware();

middleware.use(function(next) {
    var self = this;
    setTimeout(function() {
        self.hook1 = true;
        next();
    }, 10);
});

middleware.use(function(next) {
    var self = this;
    setTimeout(function() {
        self.hook2 = true;
        next();
    }, 10);
});

var start = new Date();
middleware.go(function() {
    console.log(this.hook1); // true
    console.log(this.hook2); // true
    console.log(new Date() - start); // around 20
});
```

More info: http://expressjs.com/en/guide/using-middleware.html

